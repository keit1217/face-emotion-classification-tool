# Face Emotion Classification Tool

此工具將人臉情緒分為五大類：

- happiness
- sadness
- fear
- surprise
- anger

可以快速讓使用者分類人臉情緒並將圖片移到對應的資料夾。
## 使用教學
### step1
按下browse，並選擇圖片的資料夾

### step2
而依照圖片的人臉情緒按下對應的按鍵，
各情緒對應的按鍵分別為：

- happiness：q
- sadness：w
- fear：e
- surprise：r
- anger：t

按下對應的按鍵後就會自動將圖片移入各情緒的資料夾。